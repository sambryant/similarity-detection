# similarity-detection

An ML based python project to quickly detect near-duplicate images. This is a hobby project pursued to learn more about image classification and ML in general. I'd rather do the research myself and understand what's going into the results than just be given working code I don't fully grasp.

## Overview

Idea is to train a neural net to take pairs of images and determine if they are representing the same underlying image. If they are, we say they are "duplicates".

Throughout this we will refer to images in each pair as the original and duplicate. This is not meant to reflect any true causality about which came from which, it's just convenient.

The concept of what exactly a duplicate is is not well-defined and may require non-binary classification to handle correctly. However, there are some obvious cases which are duplicates.

**Tier 1**: Unambiguous duplicates - these could be captured with a basic norm square comparison

  1. When rescaling the original reproduces the duplicate image (preserving aspect ratio).
  2. When compressing original produces duplicate.
  3. When changing the image format of original produces the duplicate.
  4. When a small amount of Gaussian noise produces the duplicate.

**Tier 2**: Still duplicate in some sense - these are the ones machine learning is needed for.

  1. When they are the same up to a minor crop or shift
  2. When they are the same up to minor shifts in tint and brightness
  3. When they are the same up to the presence of a watermark or minor feature changes

**Tier 3**: Would be useful to capture, but aiming for these might not be good in phase 1

  1. When one is a stretched version of the the other
  2. Major crop: When one is a subset of the other or they are both crops from same parent image
  3. When one is colorized and the other is black and white
  4. When some features of the image have been majorly changed, i.e. photoshopped

## Plan

The idea is to build a system that can construct training sets, figure out how to represent the training data as inputs to a ML algorithm, then figure out what ML algorithm to train on. These are not necessarily sequential tasks. From there we iterate and tweak.

More concretely, there are three goals here for initial phase:

#### 1. Dataset builder (*where* do we get training data from?)
  
We have to construct a system which:

  - builds training data given a set of images by applying random tweaks to the images falling under **tier 1**/**tier 2** constraints.
  - Labels output and formats it in a way that's small and easy to work with

The basics of this system are finished at the time of first commit.
  




#### 2. Feature representation (*what* do we feed into learner?)

We could just take the output of the dataset builder, pairs of 128x128 images, and feed it into some ML program, but the dimension of this problem is `3*128*128*2 ~ 100k`. The learning will perform much better if we first process and represent the inputs in a way that is conducive to the task we are trying to train.

Some basic ideas
  - PCA representation
  - Edge detection -> throw out all non-edges
  - Black and white
  - Fourier coefficients


#### 3. Training (*how* do we train our system?)

This is a research task, I'm not equipped to answer this at this point. However, I do know *CNNs* are one of the dominant methods for image data - might solve invariance problem posed by cropped
