import numpy as np
from abc import abstractmethod, ABC
from typing import Dict

EXTS = ['jpg', 'png']

CROP_SIGMA = 0.05 # mean fraction of width/height cut off during cropping

MAX_DIMS = np.array([128, 128], dtype=int) # maximum width/height of resized images


class ParameterLike(ABC):

  @abstractmethod
  def to_json(self) -> Dict[str, any]:
    pass

  @classmethod
  @abstractmethod
  def from_json(cls, d: Dict[str, any]):
    pass

