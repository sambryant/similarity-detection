import PIL.Image
import os
import numpy as np

TEST_FILE_LARGE = 'inputs/test/large.jpg'
TEST_FILE_SMALL = 'inputs/test/small.jpg'

WATERMARK_DIR = 'inputs/watermarks'

class Set():
  INPUTS = 'inputs'
  INPUTS_RAW = os.path.join(INPUTS, 'raw')
  INPUTS_RESIZED = os.path.join(INPUTS, 'resized')
  INPUTS_CROPPED = os.path.join(INPUTS, 'cropped')
  INPUTS_DATASET = os.path.join(INPUTS, 'dataset')
  INPUTS_TESTS = os.path.join(INPUTS, 'tests')

  def __init__(self, name):
    self.name = name
    self.raw = os.path.join(Set.INPUTS_RAW, name)
    self.resized = os.path.join(Set.INPUTS_RESIZED, name)
    self.cropped = os.path.join(Set.INPUTS_CROPPED, name)
    self.tests = os.path.join(Set.INPUTS_TESTS, name)
    self.dataset = os.path.join(Set.INPUTS_DATASET, name)
    self.dataset_file = os.path.join(Set.INPUTS_DATASET, '%s.json' % name)

SET_1 = Set('set-1')

if __name__ == '__main__':
  im = PIL.Image.open(TEST_FILE_SMALL)
  print(im.size)
  print(np.array(im.getdata()).shape)
  print(im.getdata()[0][0])
  print(im.getpixel((0, 0)))