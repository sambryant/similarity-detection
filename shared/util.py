import PIL.Image
import os
import numpy as np
from shared.sets import *
from shared.common import *

def add_image_noise(im: PIL.Image, color_sigma: float):
  """
  Adds noise to color of each pixel of image. Color_sigma is the sigma of normal distribution
  where color representation is 256.
  """
  w, h = im.size
  noise = np.random.normal(size=(w*h, 3), scale=color_sigma)
  d_old = np.array(im.getdata())
  d_new = (d_old + noise).astype(int)
  small = d_new < 0
  large = d_new >= 256
  d_new = (((~small)*(~large)) * d_new + (large * 255))
  d_new = [tuple(c) for c in d_new]
  im.putdata(d_new)

def get_images(input_dir):
  files = os.listdir(input_dir)
  files = [f for f in files if os.path.splitext(f)[1][1:] in EXTS]
  files = [os.path.join(input_dir, f) for f in files]
  return files

def get_file_extensions(input_dir):
  files = os.listdir(input_dir)
  exts = set()
  for f in files:
    path, ext = os.path.splitext(f)
    exts.add(ext)
  print('Found %d extensions: %s' % (len(exts), ', '.join([e for e in exts])))

def count_images(input_dir):
  files = os.listdir(input_dir)
  files = [f for f in files if os.path.splitext(f)[1][1:] in EXTS]
  print('Found %d matching images' % len(files))

def make_dir(d):
  if not os.path.exists(d):
    os.makedirs(d)

if __name__ == '__main__':
  add_image_noise(PIL.Image.open(TEST_FILE_SMALL), 10)
  # get_file_extensions(SET_1['RAW'])
  # count_images(SET_1['RAW'])
