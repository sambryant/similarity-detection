"""
Adds small random tweaks to images to produce similar images (quasi-duplicates)
"""
import progressbar
import numpy as np
import PIL
from abc import ABC, abstractmethod, ABCMeta
from PIL import Image, ImageEnhance, ImageFilter
from typing import Dict

import shared.common as common
import shared.util as util
import shared.sets as sets

_IMAGE_SHIFTER_CLASSES = {}

class ImageModifier(common.ParameterLike):

  @abstractmethod
  def shift_image(self, im: PIL.Image) -> PIL.Image:
    pass

  def to_json(self) -> Dict[str, any]:
    return {'classlabel': self.__class__.classlabel()}

  @classmethod
  def from_json(cls, d: Dict[str, any]):
    return _IMAGE_SHIFTER_CLASSES[d.pop('classlabel')].from_json(d)

  @classmethod
  @abstractmethod
  def classlabel(cls) -> str:
    pass

class Cropper(ImageModifier):

  def __init__(self, crop_sigma=0.05):
    self.crop_sigma = crop_sigma

  @classmethod
  def classlabel(cls):
    return 'cropper'

  def to_json(self) -> Dict[str, any]:
    d = super().to_json()
    d['crop_sigma'] = self.crop_sigma
    return d

  @classmethod
  def from_json(cls, d: Dict[str, any]):
    return cls(**d)

  def shift_image(self, im: PIL.Image) -> PIL.Image:
    dims = np.array(im.size)
    crop_fracs = np.abs(np.random.normal(size=(2), scale=self.crop_sigma))
    crop_props = np.random.random(2)
    crop_1 = crop_props[:]*crop_fracs[:]*dims[:] # x1, y1 of cropped image
    crop_2 = dims[:]*(1 - (1-crop_props[:])*crop_fracs[:])
    im_new = im.crop((crop_1[0], crop_1[1], crop_2[0], crop_2[1]))
    return im_new

class Brightness(ImageModifier):

  def __init__(self, brightness_frac_sigma=0.05):
    self.brightness_frac_sigma = brightness_frac_sigma

  @classmethod
  def classlabel(cls):
    return 'brightness'

  def shift_image(self, im: PIL.Image) -> PIL.Image:
    brightness_shift = np.random.normal(scale=self.brightness_frac_sigma)
    enhancer = ImageEnhance.Brightness(im)
    return enhancer.enhance(1 + brightness_shift)

  def to_json(self) -> Dict[str, any]:
    d = super().to_json()
    d['brightness_frac_sigma'] = self.brightness_frac_sigma
    return d

  @classmethod
  def from_json(cls, d: Dict[str, any]):
    return cls(**d)

class Watermark(ImageModifier):
  dim_frac_min: float # Minimum possible fraction of width/height watermark can occupy
  dim_frac_max: float # Maximum possible fraction of width/height watermark can occupy
  noise_strength: float # Strength of gaussian noise applied to each pixel on watermark
  watermark_source_dir: str # The directory to source watermark images from

  def __init__(self, watermark_source_dir: str=sets.WATERMARK_DIR, noise_strength: float=4.0, dim_frac_min: float=0.01, dim_frac_max: float=0.07):
    self.dim_frac_min = dim_frac_min
    self.dim_frac_max = dim_frac_max
    self.noise_strength = noise_strength
    self.watermark_source_dir = watermark_source_dir
    self.watermark_image_files = util.get_images(watermark_source_dir)

  @classmethod
  def classlabel(cls):
    return 'watermark'

  def shift_image(self, im: PIL.Image) -> PIL.Image:
    im_dims = np.array(im.size)

    # Randomly select the dimensions of the watermark based on fractions of total image size
    rands = np.random.random(2)
    wm_dims = im_dims * (self.dim_frac_min + rands*(self.dim_frac_max - self.dim_frac_min))
    wm_dims = [int(wm_dims[0]), int(wm_dims[1])]

    # Randomly choose the watermark image
    r = np.random.randint(low=0, high=len(self.watermark_image_files))
    wm_img = Image.open(self.watermark_image_files[r])
    wm_img = wm_img.resize(wm_dims)

    # Randomly add noise to watermark
    util.add_image_noise(wm_img, self.noise_strength)

    # Randomly choose the location and offset
    lower = np.random.random(2) < 0.5
    offset = (np.random.random(2) * 4).astype(int)
    place = ((lower * offset) + ~lower * (im_dims - offset - wm_dims)).astype(int)
    im.paste(wm_img, (place[0], place[1]))
    return im

  def to_json(self) -> Dict[str, any]:
    d = super().to_json()
    params = ['noise_strength', 'dim_frac_min', 'dim_frac_max', 'watermark_source_dir']
    for k in params:
      d[k] = getattr(self, k)
    return d

  @classmethod
  def from_json(cls, d: Dict[str, any]):
    return cls(**d)

# Create registry of all known image shifting classes
for cls in ImageModifier.__subclasses__():
  _IMAGE_SHIFTER_CLASSES[cls.classlabel()] = cls
