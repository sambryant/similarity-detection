"""
Creates a set of 'tweaked' and 'original' images from a raw image set.

Schematically:

                                                                    ┌──────────────────────┐
                                                                    │  ***   DATASET   *** │
 ┌──────────────┐      ┌───────────────────┐                        │                      │
 │ * RAW IMGS * │      │ * PRE-PROCESSOR * ├──┬────────────────────>│ entry 1 (unmodified) │
 │              │      │                   │  │ ┌──────────────┐    │                      ├─ ML ?
 │              ├─────>│                   │  │ │ * MODIFIER * ├───>│ entry 2 (tweaked)    │
 │    large     │      │  shrinks images   │  └>│  makes rand  │    │                      │
 │ similar dims │      │  randomly crops   │    │    tweaks    ├───>│ entry 3 (tweaked)    │
 └──────────────┘      └───────────────────┘    └──────────────┘    └──────────────────────┘

Pre-Processing: here the original images are shrunk and randomly cropped.

Modifier: System generates versions of preprocessed images with random modifications.

Dataset: Both the unmodified preprocessed and tweaked images are put into a labeled dataset.

The end result is a directory with an assortment of 'originals' and 'tweaked' images along with a
JSON file that labels which is which.

This is intended to be the input to some ML program, or further preprocessing, like PCA.
"""
from __future__ import annotations

import json, os, progressbar, shutil, uuid
import numpy as np
from abc import ABC, abstractmethod
from PIL import Image
from typing import Dict, Optional, List

import shared.sets as sets
import shared.util as util
import dataset_builder.modifier as modifier
from shared.common import ParameterLike
from shared.sets import Set
from dataset_builder.modifier import ImageModifier
from dataset_builder.preprocessor import PreProcessor

class Entry(ParameterLike):
  """
  Represents a single image in a dataset.
  """
  source_id: int # the id corresponding to the original source image
  uuid: str # a unique UUID identifying this particular entry
  file: str # the path of the file in the dataset folder
  modifier: str # a string indicating how this image was tweaked, or None if it is not tweaked.

  def __init__(self, source_id: int, uuid: str, file: str, modifier: str=None):
    self.source_id = source_id
    self.uuid = str(uuid)
    self.file = str(file)
    self.modifier = str(modifier)

  def to_json(self) -> Dict[str, any]:
    return {
      'source_id': self.source_id,
      'uuid': self.uuid,
      'file': self.file,
      'modifier': self.modifier
    }

  @classmethod
  def from_json(cls, d: Dict[str, any]) -> Entry:
    return cls(**d)

class Dataset(ParameterLike):
  fileset: Set
  limit: Optional[int]
  preprocessor: PreProcessor
  dupe_generators: List[ImageModifier]

  id_to_source: Dict[int, str]
  dataset: List[Entry]
  uuid_to_index: Dict[str, int]
  uuid_to_dupe_uuids: Dict[str, List[str]]
  raw_source_dir: str
  prc_source_dir: str
  dataset_dir: str

  def __init__(self, fileset: Set, preprocessor: PreProcessor, limit=None):
    self.fileset = fileset
    self.limit = limit
    self.preprocessor = preprocessor
    self.dupe_generators = []

    self.id_to_source = {}
    self.dataset = []
    self.uuid_to_index = {}
    self.uuid_to_dupe_uuids = {}
    self.raw_source_dir = self.fileset.raw
    self.prc_source_dir = None
    self.dataset_dir = None

    self.initialized = False
    self.fresh = True

  def add_dupe_generator(self, dg: ImageModifier):
    self.dupe_generators.append(dg)
    return self

  def make_fresh(self, show_progress=True):
    if len(self.dupe_generators) == 0:
      raise Exception('Must add a dupe generator before building a dataset')

    self.fresh = True
    self.initialized = False

    print('1. Preprocessing raw files...')
    self.prc_source_dir = self.preprocessor.go(self.fileset, show_progress=show_progress, limit=self.limit)
    self._label_sources()
    self._prepare_dataset()
    print('2. Adding originals to dataset...')
    self._add_originals_to_dataset()
    print('3. Adding duplicates to dataset...')
    self._add_duplicates_to_dataset()
    self._finalize_relations()
    self._write_label_file()
    print('Created dataset with record file: %s' % self.fileset.dataset_file)

    self.initialized = True

  def _label_sources(self):
    # Assign each source file a sequential ID and store in a map
    source_files = util.get_images(self.prc_source_dir)
    for i, f in enumerate(source_files):
      self.id_to_source[i + 1] = f

  def _prepare_dataset(self):
    self.dataset_dir = self.fileset.dataset
    if os.path.exists(self.dataset_dir):
      shutil.rmtree(self.dataset_dir)
    util.make_dir(self.dataset_dir)

  def _add_new_entry(self, source_id, dupe_generator: ImageModifier=None):
    ext = os.path.splitext(self.id_to_source[source_id])[1]
    uid = str(uuid.uuid4())
    dset_file = os.path.join(self.dataset_dir, '%s%s' % (uid, ext))
    modifier = dupe_generator and dupe_generator.classlabel() or None

    self.uuid_to_index[uid] = len(self.dataset)
    self.dataset.append(Entry(source_id, uid, dset_file, modifier=modifier))
    return dset_file

  def _add_originals_to_dataset(self, show_progress=True):
    itr = self.id_to_source.items()
    if show_progress:
      itr = progressbar.progressbar(itr)

    for sid, source_file in itr:
      dset_file = self._add_new_entry(sid, None)
      shutil.copyfile(source_file, dset_file)

  def _add_duplicates_to_dataset(self, show_progress=True):
    itr = self.id_to_source.items()
    if show_progress:
      itr = progressbar.progressbar(itr)

    for sid, source_file in itr:
      im = Image.open(source_file)
      for method in self.dupe_generators:
        dset_file = self._add_new_entry(sid, method)
        im_new = method.shift_image(im.copy())
        im_new.save(dset_file)

  def _finalize_relations(self):
    """
    Make a map giving list of dupes for each file.
    """
    self.uuid_to_dupe_uuids = {}
    source_to_dupe_uuids = {}
    for entry in self.dataset:
      uuid = str(entry.uuid)
      src = self.id_to_source[entry.source_id]
      if 'source_file' not in source_to_dupe_uuids:
        source_to_dupe_uuids['source_file'] = set()
      source_to_dupe_uuids['source_file'].add(uuid)
    for _, uuids in source_to_dupe_uuids.items():
      for uuid in uuids:
        self.uuid_to_dupe_uuids[uuid] = list(uuids)
        self.uuid_to_dupe_uuids[uuid].remove(uuid) # remove self-reference

  def _write_label_file(self):
    fname = self.fileset.dataset_file
    print(fname)

    with open(fname, 'w') as f:
      json.dump(self.to_json(), f, indent=2)

  @classmethod
  def load(cls, s: Set):
    fname = s.dataset_file
    return cls.from_json(json.loads(open(fname, 'r').read()))

  def to_json(self) -> Dict[str, any]:
    return {
      'set_name': self.fileset.name,
      'limit': self.limit,
      'preprocessor': self.preprocessor.to_json(),
      'dupe_generators': [d.to_json() for d in self.dupe_generators],
      'dataset': [e.to_json() for e in self.dataset],
      'id_to_source': self.id_to_source,
      'uuid_to_index': self.uuid_to_index,
      'uuid_to_dupe_uuids': self.uuid_to_dupe_uuids,
      'raw_source_dir': self.raw_source_dir,
      'prc_source_dir': self.prc_source_dir,
    }

  @classmethod
  def from_json(cls, d: Dict[str, any]):
    s = Set(d['set_name'])
    b = Dataset(s, PreProcessor.from_json(d['preprocessor']), limit=d['limit'])

    b.dupe_generators = [ImageModifier.from_json(dd) for dd in d['dupe_generators']]

    b.dataset = [Entry.from_json(dd) for dd in d['dataset']]
    b.id_to_source = d['id_to_source']
    b.uuid_to_index = d['uuid_to_index']
    b.uuid_to_dupe_uuids = d['uuid_to_dupe_uuids']
    b.prc_source_dir = d['prc_source_dir']
    b.raw_source_dir = d['raw_source_dir']
    b.fresh = False
    b.initialized = True
    return b

def test_make_fresh(s: Set):
  pp = PreProcessor(max_size=256)
  b = Dataset(s, pp, limit=10)\
    .add_dupe_generator(modifier.Cropper())\
    .add_dupe_generator(modifier.Brightness())\
    .add_dupe_generator(modifier.Watermark())\
    .make_fresh()

def test_load(s: Set):
  b = Dataset.load(s)
  b.fileset.dataset_file += '_test_changed'
  b._write_label_file()

if __name__ == '__main__':
  test_make_fresh(sets.SET_1)
  # test_load(sets.SET_1)
