"""
Takes images and reduces their size to fix below a threshold
"""
import os, sys
import numpy as np
import progressbar
import shutil
from PIL import Image
from numpy import array
from typing import Dict

import shared.common as common
import shared.util as util
from shared.sets import *

CROP_SIGMA = 0.05 # mean fraction of width/height cut off during cropping
MAX_SIZE = 128 # maximum width/height of resized images

class PreProcessor(common.ParameterLike):

  def __init__(self, crop_sigma=CROP_SIGMA, max_size=MAX_SIZE):
    self.crop_sigma = float(crop_sigma)
    self.max_size = int(max_size)

  def go(self, s: Set, show_progress=True, limit=None) -> str:
    if os.path.exists(s.cropped):
      shutil.rmtree(s.cropped)
    if os.path.exists(s.resized):
      shutil.rmtree(s.resized)
    crop_set(s.raw, s.cropped, crop_sigma=self.crop_sigma, show_progress=show_progress, limit=limit)
    resize_set(s.cropped, s.resized, max_size=self.max_size, show_progress=show_progress, limit=limit)
    return s.resized

  def to_json(self) -> Dict[str, any]:
    return {
      'crop_sigma': self.crop_sigma,
      'max_size': self.max_size,
    }

  @classmethod
  def from_json(cls, d: Dict[str, any]):
    return cls(**d)

def _do_for_images(input_dir, process_function, show_progress=True, limit=None):
  inputs = util.get_images(input_dir)
  if limit and len(inputs) > limit:
    inputs = inputs[0:limit]
  if show_progress:
    inputs = progressbar.progressbar(inputs)
  for f in inputs:
    im = Image.open(f)
    process_function(im, f)

def crop_set(input_dir, output_dir, crop_sigma, show_progress=True, limit=None):
  def crop_image(im, f):
    f_new = os.path.join(output_dir, os.path.basename(f))
    dims = np.array(im.size)
    crop_fracs = np.abs(np.random.normal(size=(2), scale=crop_sigma))
    crop_props = np.random.random(2)
    crop_1 = crop_props[:]*crop_fracs[:]*dims[:] # x1, y1 of cropped image
    crop_2 = dims[:]*(1 - (1-crop_props[:])*crop_fracs[:])
    im_new = im.crop((crop_1[0], crop_1[1], crop_2[0], crop_2[1]))
    im_new.save(f_new)

  util.make_dir(output_dir)
  _do_for_images(input_dir, crop_image, show_progress=show_progress, limit=limit)

def resize_set(input_dir, output_dir, max_size, show_progress=True, limit=None):
  def resize_image(im, f):
    dims = np.array(im.size)

    if dims[0] > max_size or dims[1] > max_size:
      overflow = dims[:] / max_size
      if overflow[0] > overflow[1]:
        new_dims = [max_size, int(np.round(max_size/dims[0] * dims[1]))]
      else:
        new_dims = [int(np.round(max_size/dims[1] * dims[0])), max_size]
      im_new = im.resize(new_dims)

    f_new = os.path.join(output_dir, os.path.basename(f))
    im_new.save(f_new)

  util.make_dir(output_dir)
  _do_for_images(input_dir, resize_image, show_progress=show_progress, limit=limit)
